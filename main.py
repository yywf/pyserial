#-*- coding: UTF-8 -*-
import sys
from PyQt4 import QtGui
from PySerial import PySerial

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    ps = PySerial()    
    ps.show()  
    app.aboutToQuit.connect(ps.onQuit)  
    sys.exit(app.exec_())
